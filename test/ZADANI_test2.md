Vytvořte rezervační systém pro čtenáře knihovny.
- Knihovna má k dispozici několik kopií každé knihy. 
- Každá kniha má svůj název, autora, rok vydání a zároveň každý kniha může být různého typu (např. audiokniha, e-kniha, tištěná kniha).
- Každý typ knihy má svoje unikátní parametry (alespoň 2) př. (tištěná kniha - vazba, audiokniha - délka stopáže, e-kniha - formát)
- Čtenáři (uživatelé) si mohou půjčit knihu, pokud je dostupná. Pokud není, mohou si ji rezervovat. Zároveň pokud je k dispozici více kopií, může si ji půjčit více čtenářů.
- Ve chvíli kdy je kniha v knihovně dostupná (vrátila se, nebo se naskladnila nová), je uživatel informován o tom, že si ji může vyzvednout. V případě více uživatelů mající knihu rezervovanou, jsou informování všichni. 
- Uživatel může knihu vrátit - v knihovně je dostupná kopie a všichni, kteří si ji rezervovali, budou upozorněni na její dostupnost.
- Určete, jaké designové vzory použijete pro implementaci rezervačního systému a vytváření různých typů knih a aplikujte je.

- Vytvořte model knihovny a implementujte všechny potřebné metody. Pro výpis informací použijte standardní výstup (System.out).
    - Vytvořte 4 Knihy, K1 bude tištěná kniha, K2 bude audiokniha a K3 bude e-kniha a K4 bude audiokniha stejná jako K1. Každou po jednom kuse.
    - Vytvořte 3 uživatele U1, U2, U3
    - Uživatel U1 si půjčí knihu K1
    - Uživatel U2 a U3 si chce půjčit K1, ale je půjčena
    - Uživatel U2 a U3 si knihu K1 rezervují.
    - Uživatel U1 vrací K1
    - Uživatel U2 a U3 jsou informováni a možnosti si půjčit K1
    - Uživatel U2 si půjčí K1
    - Uživatel U3 si půjčí K4

Bonus:
    Pokud je stejná kniha dostupná také v jiné verzi - navrhněte uživateli alternativu. Např. pokud chce audioknihu, ktera je rezervovaná, nabídněte mu tištěnou knihu.