# F7PMIOOP_2023_par2

## Java vs JRE vs JDK

### Java
Java is a high-level, versatile, and widely used programming language. It is known for its platform independence, as Java code can be written once and run on various platforms without modification. Java programs are compiled into bytecode, which can be executed on any system with a compatible Java Virtual Machine (JVM). Java is used for a wide range of applications, from web development to mobile app development and more.

### JRE (Java Runtime Environment)
The Java Runtime Environment (JRE) is a software package that provides the necessary runtime environment for executing Java applications and applets. It includes the Java Virtual Machine (JVM), class libraries, and other runtime components. JRE is essential for running Java applications on a computer. Users who want to run Java software only need to install the JRE, as it does not include development tools.

### JDK (Java Development Kit)
The Java Development Kit (JDK) is a software package that includes everything needed for Java development. It contains the JRE for running Java applications and also includes development tools such as the Java Compiler (`javac`), debugger, and various utilities. Developers use the JDK to write, compile, and package Java applications and applets. It is necessary for creating Java software and is typically used by developers, not end-users.

In summary:
- **Java** is the programming language itself.
- **JRE (Java Runtime Environment)** is required to run Java applications and includes the JVM and libraries.
- **JDK (Java Development Kit)** is used by developers to create and compile Java applications and includes the JRE along with development tools.

Understanding the distinctions between these components is essential for both using and developing Java applications.

## Git Cheat Sheet

### Basic Commands

#### Initialize a New Repository
```bash
git init
```

#### Clone a Repository
```bash
git clone <repository_url>
```

#### Add Files to Staging Area
```bash
git add <file(s)>
```

#### Commit Changes
```bash
git commit -m "Commit message"
```

### Branching

#### Create a New Branch
```bash
git branch <branch_name>
```

#### Switch to a Branch
```bash
git checkout <branch_name>
```

#### Create and Switch to a New Branch
```bash
git checkout -b <branch_name>
```

#### List Branches
```bash
git branch
```

#### Delete a Branch (locally)
```bash
git branch -d <branch_name>
```

### Remote Repositories

#### Fetch Changes from a Remote Repository
```bash
git fetch <remote_name>
```

#### Pull Changes from a Remote Repository
```bash
git pull <remote_name> <branch_name>
```

#### Push Changes to a Remote Repository
```bash
git push <remote_name> <branch_name>
```

### Merging and Rebasing

#### Merge a Branch into Current Branch
```bash
git merge <branch_name>
```

#### Rebase Current Branch onto Another Branch
```bash
git rebase <base_branch_name>
```

### Undoing Changes

#### Reset to a Previous Commit
```bash
git reset <commit_hash>
```

### Viewing History

#### View Commit History
```bash
git log
```

#### Show Changes in a Specific Commit
```bash
git show <commit_hash>
```

### Miscellaneous

#### Show the Status of the Working Directory
```bash
git status
```

#### View Git Configuration
```bash
git config --list
```

#### Help
```bash
git --help
git <command> --help
```

